import resource
from functools import wraps
from time import sleep

from concurrent.futures import ThreadPoolExecutor


class MemoryMonitor:

    def __init__(self, freq):
        self.keep_measuring = True
        self.freq = freq

    @staticmethod
    def _format_size(B):
        B = float(B)
        KB = float(1024)
        MB = float(KB ** 2) # 1,048,576
        GB = float(KB ** 3) # 1,073,741,824
        TB = float(KB ** 4) # 1,099,511,627,776

        if B < KB:
            return '{0} {1}'.format(B,'Bytes' if 0 == B > 1 else 'Byte')
        elif KB <= B < MB:
            return '{0:.2f} KB'.format(B/KB)
        elif MB <= B < GB:
            return '{0:.2f} MB'.format(B/MB)
        elif GB <= B < TB:
            return '{0:.2f} GB'.format(B/GB)
        elif TB <= B:
            return '{0:.2f} TB'.format(B/TB)

    def measure_usage(self, format=True):
        max_usage = 0
        while self.keep_measuring:

            max_usage = max(
                max_usage,
                resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
            )
            sleep(self.freq)
        if format:
            max_usage = self._format_size(max_usage)
        return max_usage


def imredi_memory_profile(freq=.1):

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            with ThreadPoolExecutor() as executor:
                monitor = MemoryMonitor(freq)
                mem_thread = executor.submit(monitor.measure_usage)
                try:
                    fn_thread = executor.submit(func)
                    result = fn_thread.result()
                finally:
                    monitor.keep_measuring = False
                    max_usage = mem_thread.result()
                wrapper.max_usage = max_usage
                print(f"Peak memory usage for {func.__name__}: {max_usage}")
            return result
        return wrapper
    return decorator

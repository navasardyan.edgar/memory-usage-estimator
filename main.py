import resource
import statistics

from profiling import imerdi_profile
import time


def target_func():
    rs = 1
    a = [10] * (11 ** 8)
    # time.sleep(2)
    return rs


@imerdi_profile(.01)
def decorated():
    target_func()


@imerdi_profile(.01)
def double_decorated():
    decorated()


if __name__ == '__main__':

    decorated()
    x = decorated.max_usage

    double_decorated()
    y = double_decorated.max_usage
    delta = (abs((y-x)/x)*100)
    print(delta)
